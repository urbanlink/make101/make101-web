const path = require(`path`)

/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

exports.createPages = async ({ actions, graphql, reporter }) => {
  
  const { createPage } = actions

  // Create main home page
  createPage({
    path: `/`,
    component: path.resolve(`./src/templates/index.js`)
  })
  
  // Create list wildcard page
  createPage({
    path: "/list",
    matchPath: "/list/:listSlug",
    component: path.resolve('./src/templates/list.js')
  })
  
  // Create item wildcard page
  createPage({
    path: "/item",
    matchPath: "/item/:itemSlug",
    component: path.resolve('./src/templates/item.js')
  })

  // Create main home page
  createPage({
    path: `/edit/item`,
    matchPath: '/edit/item/:itemSlug',
    component: path.resolve(`./src/templates/editItem.js`)
  })

  const blogPostTemplate = path.resolve(`src/templates/article.js`)
  const result = await graphql(`
    {
      allMarkdownRemark(
        sort: { order: DESC, fields: [frontmatter___date] }
        limit: 1000
      ) {
        edges {
          node {
            frontmatter {
              path
            }
          }
        }
      }
    }
  `)
  // Handle errors
  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }
  result.data.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      path: node.frontmatter.path,
      component: blogPostTemplate,
      context: {}, // additional data can be passed via context
    })
  })
}

// // Implement the Gatsby API “onCreatePage”. This is
// // called after every page is created.
// exports.onCreatePage = async ({ page, actions }) => {
//   const { createPage } = actions

//   // page.matchPath is a special key that's used for matching pages
//   // only on the client.
//   // if (page.path.match(/^\/list/)) {
//   //   page.matchPath = "/list/*"

//   //   // Update the page.
//   //   createPage(page)
//   // }
// }