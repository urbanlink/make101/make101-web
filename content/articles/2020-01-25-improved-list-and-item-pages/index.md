---
type: article
title: Improved list and item pages
path: "/article/improved-list-and-item-pages"
date: 2020-01-25T00:00:00+01:00
draft: false
---
Today I released v0.3.0 of make101! 

With this release the lists and items get more attention. Each list and item has its own page. For instance, check out my list: https://make101.urbanlink.nl/list/arn or one of my items: https://make101.urbanlink.nl/item/create-an-i-spindle-brew-gravity-meter-https-github-com-universam1-i-spindel-48 

With each make item on its own pages, it is possible to add new features in the future: such as commenting on items, subscribing to items, easy sharing of items on social media etc. 

Did you share your list? Make sure you tweet about it! #make101

Happy making!

Arn

@_avanderpluijm