---
type: article
title: Improved widget sharing
path: "/article/improved-widget-sharing"
date: 2020-01-06T00:00:00+01:00
draft: false
---
A new year, new possibilities for making! And now it is even easier to share your list with the world! Because that is what I find interesting, share your ideas for making as early as possible. The more people know what you want to do, the better! Together we can reach a lot more than by yourself.

with the latest update I created a better widget sharing experience. Take a look at the frontpage of urbanlink.nl, where I share my own list. You can find out how to share your list at your share page.

Sharing is as easy by adding two lines of code to your site (where you replace {{YOUR_KEY}} with the key for your account: 

    <div id="make101_list" data-m101_key={{YOUR_KEY}}></div>
    <script async src="https://make101.urbanlink.nl/embed/embed.js" charset="utf-8"></script>

You can find you m101_key at your [share](https://make101.urbanlink.nl/share) page. 

You can override the default options by adding the following data properties to the div-element: 

* width: minimal 220 (default)
* height: minimal 400 (default)
* sort: title, created(default), random
* filter: none(default), to make, done

For example: 

    <div id="make101_list" 
    	data-m01_key="{{YOUR_KEY}}"
        data-width="400"
        data-height="800"
        data-sort="random"
        data-filter="done"></div>
    <script async src="https://make101.urbanlink.nl/embed/embed.js" charset="utf-8"></script>

Did you share your list? Make sure you tweet about it! #make101

Happy making!

Arn

@_avanderpluijm