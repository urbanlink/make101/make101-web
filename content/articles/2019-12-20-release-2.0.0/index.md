---
type: article
title: "Version 0.2.0 released"
path: /article/version-0.2.0-released
date: 2019-12-21
---
This marks the release of version 0.2.0 of MAKE101. This is a major change from previous release. The frontend is changed from using Ionic/Angular to a JAM stack using GatsbyJS (based on react). 

GatsbyJS makes it very easy, and more efficient to create these types of applications. I don't need to create a separate version for mobile use anymore and the react framework is a lot more developer friendly than Angular. 

With this release it is still possible to create your own MAKE101 list of 101 items that you are going to make. It is also possible to share this list with others, by embedding your list on your own site. Check out the details on the [Share page](/share).

Start your list today! 
