// Fetch make items for a given user-sub 
// url request: //api.make101.urbanlink.nl/widgets/listing/widget-listing.html?key=${m101_key}


// Wait for loading to be complete 
window.onload = function(e) {

  console.log('app.js loaded')
  let l = window.location.search.substr(1).split('&');
  o={}
  l.forEach(n => {
    n = n.split('=');
    o[n[ 0]] = n[ 1]
  })
  console.log(o)

  var m101_key = o.m101_key; 
  if (!o.m101_key) { 
    console.warn('no key provided'); 
    document.getElementById('empty').innerHTML='Error: No key provided.'; 
    return; 
  }
  
  // Define the key 
  // var url = window.location.origin + '/make?key=' + key;
  var url = 'https://api.make101.urbanlink.nl/item?key='+m101_key;
  this.console.log(url)

  var xhr = new XMLHttpRequest()
      xhr.open('GET', url, true)
      xhr.withCredentials = false
      xhr.setRequestHeader('Content-Type', 'application/json')
      xhr.send()

  xhr.onreadystatechange = function() {
    if (xhr.readyState === 4) {
      try {
        var r = JSON.parse(xhr.response);
        handleResult(r);
      } catch(err) {
        console.warn(err);
        document.getElementById('empty').innerHTML='Error: There was an error parsing the items.'; 
      }
    }
  }

  function handleResult(items) {
    
    if (!items || (items.length === 0)) {
      console.log('no items')
      document.getElementById('empty').innerHTML='There are no items created yet.'; 
    } else {

      var ul = document.getElementById('list');
      for (var i = 0; i < items.length; i++) {
        var li = document.createElement('li');
        li.appendChild(document.createTextNode(items[i].title));
        ul.appendChild(li);
      }
    }
  }
}
  