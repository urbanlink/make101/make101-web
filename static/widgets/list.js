
const supportedAPI = ['init']; // enlist all methods supported by API (e.g. `mw('event', 'user-login');`)

function app(window) {
  console.log('M101ListObject starting');

  // set default configurations
  let configurations = {
      someDefaultConfiguration: false
  };

  // all methods that were called till now and stored in queue
  // needs to be called now 
  let globalObject = window[window['M101ListObject']];
  console.log(globalObject)
  let queue = globalObject.q;
  if (queue) {
    for (var i = 0; i < queue.length; i++) {
      if (queue[i][0].toLowerCase() == 'init') {
          configurations = extendObject(configurations, queue[i][1]);
          console.log('M101ListObject started', configurations);
      }
      else {
        // apiHandler(queue[i][0], queue[i][1]);
      }
    }
  }

  // override temporary (until the app loaded) handler
  // for widget's API calls
  // globalObject = apiHandler;
  // globalObject.configurations = configurations;
  
}

app(window);