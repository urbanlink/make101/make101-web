// https: //github.com/tastejs/todomvc/blob/gh-pages/examples/react/js/todoItem.jsx

import React from 'react'; 
import styled from 'styled-components';
import tw from 'tailwind.macro';
import { FetchMyList } from '../../../utils/list.service'; 
import ToMakeItem from '../../makeItem/ToMakeItem';
import { CreateItem, DestroyItem, ToggleItem } from '../../../utils/list.service';
import { isBrowser, isAuthenticated } from '../../../utils/auth.service'; 

class MyList extends React.Component {

  constructor(props) {
    super(props); 
    this.state = {
      items: [],
      newItem: ''
    }
  }

  async componentDidMount() {
    if (isBrowser && isAuthenticated()) {
      try {
        const result = await FetchMyList();
        this.setState({
          items: result
        });
      } catch (err) {
        console.warn(err);
      }
    }
  }
  
  handleChange = (evt) => {
    this.setState({
      newItem: evt.target.value
    });
  }

  // Toggle the done status of an item 
  toggle = (evt) => {
    
    const id = parseInt(evt.target.value); 
    const index = this.state.items.findIndex(element => element.id === id); 
    
    ToggleItem(this.state.items[ index].id, !this.state.items[ index].done)
      .then(result => {
        let s = this.state.items;
        s[ index].done = !s[ index].done;
        this.setState({ items: s })
      })
      .catch(err => { console.warn(err); });
  }
  
  /**
   * 
   * 
   * 
   */
  destroy = (evt) => {

    const id = parseInt(evt.target.value)
    const index = this.state.items.findIndex(element => element.id === id); 
    
    DestroyItem(this.state.items[ index].id)
      .then(result => {
        let a = this.state.items;
        a.splice(index, 1); 
        this.setState({ items: a })
      })
      .catch(err => console.warn(err)); 
  }

  /**
   * 
   * 
   * 
   */
  handleKeyDown = (event) => {
    if (event.which !== 13 ) { return; }
    event.preventDefault(); 
    const val = this.state.newItem.trim(); 
    if (val) {
      CreateItem(val).then(result => {
        this.setState({newItem: ''}); 
        let a = [result]; 
        let b = this.state.items; 
        let c = a.concat(b); 
        this.setState({items: c})
      }).catch(err=> console.warn(err))
    }
  }

  edit = () => { console.log('MyList - edit') }
  save = () => { console.log('MyList - save') }

  render() {
    if (!isAuthenticated()) { return null; }
    if (!this.state.items) { return null; }
    
    return (
      <Wrapper>
        <Title>Your list of 101 things to make!</Title>
        <ToMakeApp>
          <Header>
            <CreateInput 
              onKeyDown={ this.handleKeyDown }
              value={this.state.newItem}
              placeholder="What are you going to make?"
              onChange = {this.handleChange}
            />
          </Header>
          <Main>
            <ToMakeList> {
                this.state.items.map((tomake, key) => (
                  <ToMakeItem 
                    tomake={ tomake } 
                    key={ key } 
                    onToggle={ this.toggle }
                    onDestroy={ this.destroy }
                    onEdit={ this.edit }
                    editing={ this.state.editing === tomake.id }
                    onSave={ this.save }
                    onCancel={ this.cancel }
                  />
                ))
              } 
            </ToMakeList>
          </Main>
          <Footer />
        </ToMakeApp>
      </Wrapper>
    ); 
  }
}

export default MyList; 

const Wrapper = styled.div`${tw`my-16`}`
const Title = styled.h3`${tw`text-center text-3xl`}`
const ToMakeApp = styled.div`${tw`max-w-xl m-auto bg-white relative my-8 shadow-md`}`
const Header = styled.header`${tw`h-16`}`

const CreateInput = styled.input`
  ${tw`relative w-full border-0 m-0 p-4 h-16 text-xl text-gray-600 italic leading-tight shadow-inner`}
  box-sizing: border-box;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
`
const Main = styled.section`${tw``}`
const ToMakeList = styled.ul`${tw`list-none p-0 m-0`} border-top: 1px solid #efefef`
const Footer = styled.section`${tw``}`
