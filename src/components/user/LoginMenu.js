import React, { Component } from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';
import tw from 'tailwind.macro';
import { getUserInfo } from '../../utils/user.service';
import { isBrowser, isAuthenticated } from '../../utils/auth.service';

class LoginMenu extends Component {

  constructor(props) {
    super(props);
    this.state = {
      userInfo: null,
      authenticated: null
    }
  }

  componentDidMount() {
    this.setState({
      userInfo: getUserInfo(),
      authenticated: isAuthenticated()
    })
  }

  render() {

    // Conditionalky return component
    if (!this.state.userInfo || !this.state.authenticated) {
      return <LoginBtn to='/login'>Inloggen</LoginBtn>
    }
    else if (isBrowser) {
      return (
        <Link to='/settings'>
          <Avatar alt='avatar' src={ this.state.userInfo.picture } />
        </Link>
      )
    }

    return null;
  }
}

export default LoginMenu;

// Component styles
const LoginBtn = styled(Link)`${tw`uppercase text-sm rounded border border-solid border-gray-300 px-4 py-2 no-underline text-black hover:bg-gray-200`}`
const Avatar = styled.img`${tw`rounded-full h-8 w-8 p-0 m-0 mt-1 ml-3`};`
