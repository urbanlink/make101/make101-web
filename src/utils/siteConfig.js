module.exports = {
  siteTitle: 'MAKE101',
  siteTitleAlt: 'MAKE101', // This allows an alternative site title for SEO schema.
  publisher: 'MAKE101', // Organization name used for SEO schema
  siteDescription:
    'MAKE101',
  siteUrl: 'https://make101.urbanlink.nl', // Site domain. Do not include a trailing slash! If you wish to use a path prefix you can read more about that here: https://www.gatsbyjs.org/docs/path-prefix/
  postsPerHomePage: 7, // Number of posts shown on the 1st page of of the index.js template (home page)
  postsPerPage: 6, // Number of posts shown on paginated pages
  author: 'MAKE101', // Author for RSS author segment and SEO schema
  authorUrl: 'https://urbanlink.nl', // URL used for author and publisher schema, can be a social profile or other personal site
  userTwitter: '@_avanderpluijm', // Change for Twitter Cards
  shortTitle: 'MAKE101', // Used for App manifest e.g. Mobile Home Screen
  shareImage: null, // Open Graph Default Share Image. 1200x1200 is recommended
  shareImageWidth: 900, // Change to the width of your default share image
  shareImageHeight: 600, // Change to the height of your default share image
  siteLogo: '/favicon.png', // Logo used for SEO, RSS, and App manifest
  backgroundColor: '#e9e9e9', // Used for Offline Manifest
  themeColor: '#121212', // Used for Offline Manifest
  copyright: 'Copyright © 2019 urbanlink.nl', // Copyright string for the RSS feed
}
